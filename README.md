UniteKerala
=================
[unitekerala.com](https://unitekerala.com)


INTRODUCTION
------------
The UniteKerala web app is part of UniteKerala emergency public campaign to aid
in 2018 Kerala Flood Disaster Relief Activities.


LICENSE
-------
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see
[www.gnu.org/licenses/](https://www.gnu.org/licenses/gpl).

CONTRIBUTION
------------
UniteKerala is a Free Software. All developers and contributors are volunteers
and we're always looking for new additions and resources.


CONTACT
-------
[help@unitekerala.com](mailto:help@unitekerala.com)

