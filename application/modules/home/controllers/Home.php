<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	// Protected or private properties
	protected $_template;

	public function __construct() {
		parent::__construct();
		$this->load->model('home/home_model','home');
		$this->load->language('admin/home', 'english');
	}

	public function index() {
		$data['home'] = $this->home->get_home_data();
		$data['homeslider']	= true;
		$data['page_data'] 	= array(
				'title'=> "Keralapadanam 2.0 by KSSP",
				'description' => "Keralapadanam is the socio-economic survey and study of Kerala. Kerala Padanam is conducted by Kerala Sasthrasahithya Parishad.",
													);
		$this->_template['page'] = 'home/home';
		$this->system_library->load($this->_template['page'], $data);
	}
    public function pages($pageval='about') {
//		$data['home'] = $this->home->get_home_data();
		$data['homeslider']	= false;
		$data['page_data'] 	= array(
						'title'=> "Keralapadanam 2 - ".$pageval." Page",
						'description' => "Keralapadanam is a socio-economic study of Kerala done by Kerala Sasthrasahithya Parishad. ".$pageval." Page",
													);
		$this->_template['page'] = 'home/'.$pageval;
		$this->system_library->load($this->_template['page'], $data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
