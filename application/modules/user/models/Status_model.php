<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Status_model extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->_table = $this->config->item('database_tables');
	}
  public function getmainstatus() {
    $userid     = $this->aauth->get_user()->id;
		$this->db->select('count(id) as counts, status, userid');
		$this->db->from($this->_table['useralloc']);
		$this->db->group_by('status');
    $this->db->having('userid',$userid);
    $query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->result_array();
			return $result;
		} else {
			return FALSE;
		}
	}
}
