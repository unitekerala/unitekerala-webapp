<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class User_model extends CI_Model {
	public function __construct() {
		parent::__construct();

		$this->_table = $this->config->item('database_tables');
	}

	public function get_last_login($userid) {
		$this->db->select('phonenumber,address,distid,blockid,lsgiid,kssp_unit,kssp_mekhalaid');
		$this->db->from($this->_table['profile']);
		$this->db->where('id', $userid);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->row_array();
			return $result;
		}
	}

	public function get_houses_by_userid($userid) {
		$this->db->select('s_useralloc.basicinfoid,s_useralloc.userid,s_useralloc.status');
		$this->db->from($this->_table['useralloc'] . ' s_useralloc');
		$this->db->where('s_useralloc.userid', $userid);
		$this->db->where("(s_useralloc.status = 'draft' OR s_useralloc.status = 'wip' OR s_useralloc.status = 'working')");
		$this->db->select('housenumber,housename,lsgiid,ward,familyhead');
		$this->db->join($this->_table['basicinfo'], 's_useralloc.basicinfoid = basicinfo.id');
		$this->db->select('lsgi.lsgi as lsgi, lsgi.lsgicode');
		$this->db->join($this->_table['lsgi'], 'lsgiid = lsgi.id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->result_array();
			return $result;
		} else {
			return FALSE;
		}
	}

    public function get_lsgi_by_userid($userid) {
        $this->db->select('s_useralloc.basicinfoid');
		$this->db->from($this->_table['useralloc'] . ' s_useralloc');
		$this->db->where('s_useralloc.userid', $userid);
		$this->db->where("(s_useralloc.status = 'draft' OR s_useralloc.status = 'wip' OR s_useralloc.status = 'working')");
		$this->db->join($this->_table['basicinfo'], 's_useralloc.basicinfoid = basicinfo.id');
		$this->db->select('lsgi.lsgi as lsgi, lsgi.lsgicode');
		$this->db->join($this->_table['lsgi'], 'lsgiid = lsgi.id');
        $this->db->group_by('lsgi.lsgicode');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->result_array();
			return $result;
		} else {
			return FALSE;
		}
    }

	public function get_house_by_userid($userid, $basicinfoid) {
		$this->db->select('s_useralloc.basicinfoid,s_useralloc.userid');
		$this->db->from($this->_table['useralloc'] . ' s_useralloc');
		$this->db->where('s_useralloc.userid', $userid);
		$this->db->where('s_useralloc.basicinfoid', $basicinfoid);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 1) {
			$result = $query->row_array();
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function get_house_by_basicinfoid($basicinfoid) {
		$this->db->select('s_basicinfo.housenumber,s_basicinfo.ward');
		$this->db->from($this->_table['basicinfo'] . ' s_basicinfo');
		$this->db->where('id', $basicinfoid);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->result_array();
			return $result;
		}
	}

	public function get_userstate_by_basicinfoid($basicinfoid) {

		$this->db->select('s_basicinfo.userid, s_basicinfo.id');
		$this->db->from($this->_table['basicinfo'] . ' s_basicinfo');
		$this->db->where('id', $basicinfoid);
		$this->db->limit(1);

		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			$result = $query->row_array();
			return $result;
		}
	}

	public function update_userlogin_status($form_data, $userid) {
		$this->db->where('id', $userid);
		$this->db->update($this->_table['profile'], $form_data);
		if ($this->db->affected_rows() == '1') {
			return TRUE;
		}
		return FALSE;
	}

    public function update_status($form_data, $basicinfoid) {
        $this->db->where('basicinfoid', $basicinfoid);
        $this->db->update($this->_table['useralloc'],$form_data);
        if ($this->db->affected_rows() == '1') {
            return true;
        }
        return false;
    }
}
