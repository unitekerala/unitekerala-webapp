<?php defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Controller {
    // Protected or private properties
    protected $_template;

    public function __construct() {
        parent::__construct();

        $this->load->library("Aauth");
        $this->load->library('form_validation'); //The Form Validation Loader
        $this->load->model('user/data_model', 'data');

        if($this->aauth->get_user()->username != 'ksspadmin') {
            redirect(base_url(),'refresh');
        }
    }
    public function index()
    {
        $data['homeslider'] = true;
        $data['distid'] = $this->data->get_distid_drop();
        $data['page_data'] = array(
            'title' => "Get LSG Data",
            );
        $this->_template['page'] = 'data/kpdataindex';
        $this->system_library->load($this->_template['page'], $data);
    }
   public function getps($distid = 1,$typeid='G')
    {
        $distid = $this->input->post('distid');
        $typeid = $this->input->post('typeid');

        if($typeid == 'K') {
          $blocks = $this->data->get_mekhala_by_dist($distid);
        } else {
          $blocks = $this->data->get_lsgi_by_district($distid,$typeid);
        }
        $output = NULL;
        if($typeid!='C' && $typeid!='K') {
          $output = "<option value='all'>All</option>";
        } 
        foreach ($blocks as $row) {
          if($typeid == 'K') {
            $output .="<option value='" . $row->id . "'>" . $row->mekhalaname . "</option>";
          } else {
            $output .= "<option value='" . $row->lsgicode . "'>" . $row->lsgi . "</option>";
          }
        }

        //Display LSGI dropdown
        echo $output;
    }
    public function getdata()
    {
        $data['homeslider'] = true;
        $distid = $this->input->post('distid');
        $typeid = $this->input->post('typeid');
        $panid  = $this->input->post('lsgiid');
        if($panid=='') {
            $panid='G14023';
            $typeid='G';
        }
        $data['distid'] = $this->data->get_distid_drop();

        if($typeid == 'K') {
          $data['lsgiid'] = $this->data->get_mekhala_by_dist_ar($distid);
          $data['wards'] = $this->data->get_ward_by_mekhala($panid);          
        } else {
          $data['lsgiid'] = $this->data->get_lsgi_by_district_ar($distid,$typeid);
          if($panid == "all") {
              $data['wards'] = $this->data->get_ward_by_dist($distid,$typeid);
          } else {
              $data['wards'] = $this->data->get_ward_by_lsgi($panid,$typeid);
          }
        }

        $data['distval'] =$this->input->post('distid');
        $data['typeval'] =$this->input->post('typeid');
        $data['panid'] = $this->input->post('lsgiid');

        $data['page_data'] = array(
            'title' => "Kerala Padanam Selected Houses in Panchayat",
            );

        $this->_template['page'] = 'data/warddatashow';
        $this->system_library->load($this->_template['page'], $data);
    }

    public function getmissing() {
        $lsgis = $this->data->get_all_lsgis();
        foreach($lsgis as $lsgi) {
            $wards = $this->data->get_wards($lsgi['localbody_code']);
            if(!$wards) {
                $dname = $this->data->get_district_by_id($lsgi['distid']);
                echo "<p style='color:red'>".$lsgi['localbody_code'].",".$lsgi['localbody_name'].",".$dname['district']."</p>";
            }
            //echo "<textarea>";var_dump($wards);die;
            //foreach($wards as $ward) {
                //                if(!$ward['wardcode']) {
                //    echo "<p style='color:red'>".$lsgi['localbod_code'].",".$lsgi['localbody_name'].",".$lsgi['distid']."</p></br";
            //    }
            //}
        }
    }
}
