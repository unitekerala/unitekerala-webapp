<?php  if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profile extends CI_Controller
{
    protected $_template;

    public function __construct()
    {
        parent::__construct();
        if (!$this->aauth->is_loggedin()) {
            redirect("/");
        }
        //$this->access_library->check_access();
        $this->load->library('system_library');
        $this->load->language('user/profile', 'malayalam');
        $this->load->model('user/profile_model', 'profile');
        $this->load->library("Aauth");
    }

    public function index()
    {
        $this->load->library('form_validation');  //The Form Validation Loader
        $data['username']=$this->aauth->get_user()->username;
        $data['id']=$this->aauth->get_user()->id;
        $data['email']=$this->aauth->get_user()->email;

        $data['profile'] = $this->profile->get_profile_by_id($data['id']);
        $data ['page_data'] = array(
          'title' =>'Profile'
        );
        $this->_template['page']    = 'user/profile';
        $this->system_library->load_user($this->_template['page'], $data);
    }



    public function editprofile()
    {
        $this->load->library('form_validation');  //The Form Validation Loader


        $this->form_validation->set_rules('phonenumber', 'lang:form_phonenumber', 'required|trim|xss_clean|max_length[15]');
        $this->form_validation->set_rules('address', 'lang:form_address', 'required|trim|xss_clean|max_length[200]');
        $this->form_validation->set_rules('distid', 'lang:form_district', 'required|trim|xss_clean|max_length[11]|is_natural_no_zero');
        //$this->form_validation->set_rules('blockid', 'lang:form_block', 'required|trim|xss_clean|max_length[11]|is_natural_no_zero');
        $this->form_validation->set_rules('lsgiid', 'lang:form_panchayath', 'required|trim|xss_clean|max_length[11]|is_natural_no_zero');
        $this->form_validation->set_rules('kssp_mekhalaid', 'lang:form_kssp_mekhalaid', 'required|trim|xss_clean|max_length[11]|is_natural_no_zero');
        $this->form_validation->set_rules('kssp_unit', 'lang:form_kssp_unit', 'required|trim|xss_clean|max_length[40]');
        $this->form_validation->set_error_delimiters('', '<br />');

        if ($this->form_validation->run($this) === true) {
            $form_data = array(
                            'id' => $this->aauth->get_user()->id,
                            'username' => $this->aauth->get_user()->username,
                            'phonenumber' => $this->input->post('phonenumber'),
                            'address' => $this->input->post('address'),
                            'distid' => $this->input->post('distid'),
                            //'blockid' => $this->input->post('blockid'),
                            'lsgiid' => $this->input->post('lsgiid'),
                            'kssp_unit' => $this->input->post('kssp_unit'),
                            'kssp_mekhalaid' => $this->input->post('kssp_mekhalaid'),
                        );
            if(($this->profile->edit_profile($form_data, $this->aauth->get_user()->id)) === true) {
                $this->session->set_flashdata('message', $this->lang->line('profile_successfully_edited'));
                redirect('user/profile', 'refresh');
            } else {
                $this->session->set_flashdata('message', $this->lang->line('profile_error'));
                redirect('user/profile', 'refresh');
            }
        }
        $data['username']=$this->aauth->get_user()->username;
        $data['editingid']=$this->aauth->get_user()->id;
        $data['profile'] = $this->profile->get_profile_by_id($data['editingid']);
        $data['distid'] = $this->profile->get_distid_drop();
		//$data['blockid']    = $this->profile->get_blockid_drop_dist($data['profile']['distid']);
		$data['lsgiid']     = $this->profile->get_lsgiid_drop_dist($data['profile']['distid']);
		$data['kssp_mekhalaid']     = $this->profile->get_mekhalaid_drop_dist($data['profile']['distid']);
        $data['page_data'] = array(
          'title' =>"Edit profile"
        );
        $this->_template['page'] = 'user/editprofile';
        $this->system_library->load_user($this->_template['page'], $data);
    }


    public function changepass()
    {
        $this->load->library('form_validation');  //The Form Validation Loader

        $this->form_validation->set_rules('email', 'lang:form_email', 'required|trim|xss_clean|max_length[50]');
        $this->form_validation->set_rules('password', 'lang:form_password', 'required|trim|xss_clean|max_length[64]');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        // if ($this->form_validation->run($this) === true) {
        //      $a = $this->aauth->update_user($this->input->post('id'), $this->input->post('email'), $this->input->post('password'), $this->input->post('name'));
        //     if ($a) {
        //                 $this->session->set_flashdata('message', $this->lang->line('user_successfully_edited'));
        //                 redirect('admin/user', 'refresh');
        //                 //redirect('myform/success');   // or whatever logic needs to occur
        //     } else {
        //         $this->session->set_flashdata('message', $this->lang->line('user_error'));
        //         redirect('admin/user', 'refresh');
        //     }
        // } else // failed validation proceed to post failure logic
        // {
        //     $data['user'] = $this->user->get_user_by_id($id);
        //     $data ['id']=$id;
        //
        //     $data ['user_data'] = array (
        //              'title' =>"Change Password"
        //         );
        //     //
        // // 	$data ['page_data'] = array (
        // // 			'title' =>"Add user"
        // // 		);
        //     $this->_template['page']   = 'user/changepass';
        //     $this->system_library->load($this->_template['page'], $data, true);
        // }
        $data['user'] = $this->user->get_user_by_id($id);
        $data['id']=$id;


        $this->_template['page']   = 'user/changepass';
        $this->system_library->load_user($this->_template['page'], $data, false);
    }
    public function blocks($distid = 1) {
		// $this->load->library('form_validation'); //The Form Validation Loader
		// $this->form_validation->set_rules('distid', 'lang:form_district', 'required|trim|xss_clean|max_length[11]|is_natural_no_zero');
		// if (($this->form_validation->run($this)) === true) {
		$distid = $this->input->post('distid');
		// echo $distid . "Test";
		$blocks = $this->profile->get_lsgi_by_district($distid);
		$output = null;
		foreach ($blocks as $row) {
			$output .= "<option value='" . $row->id . "'>" . $row->lsgi . "</option>";
		}
		echo $output;
		// }
	}
    public function blocklist($distid=1)
    {
        $distid = $this->input->post('distid');
        $blocks = $this->profile->get_blocks_by_district($distid);
		$output = null;
		foreach ($blocks as $row) {
			$output .= "<option value='" . $row->id . "'>" . $row->block . "</option>";
		}
		echo $output;
    }

	public function getmekhala($distid = 1) {
		// $this->load->library('form_validation'); //The Form Validation Loader
		// $this->form_validation->set_rules('distid', 'lang:form_district', 'required|trim|xss_clean|max_length[11]|is_natural_no_zero');
		// if (($this->form_validation->run($this)) === true) {
		$distid = $this->input->post('distid');
		// echo $distid . "Test";
		$blocks = $this->profile->get_mekhala_by_district($distid);
		$output = null;
		foreach ($blocks as $row) {
			$output .= "<option value='" . $row->id . "'>" . $row->mekhalaname . "</option>";
		}
		echo $output;
		// }
	}

}
