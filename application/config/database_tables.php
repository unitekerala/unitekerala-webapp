<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
|--------------------------------------------------------------------------
| Database table settings
|--------------------------------------------------------------------------
|
| Table names
|
 */

$config['home'] = 'home';
$config['footer'] = 'footer';
$config['categories'] = 'categories';
$config['comments'] = 'comments';
$config['languages'] = 'languages';
$config['links'] = 'links';
$config['navigation'] = 'navigation';
$config['pages'] = 'pages';
$config['posts'] = 'posts';
$config['posts_to_categories'] = 'posts_to_categories';
$config['settings'] = 'settings';
$config['tags'] = 'tags';
$config['tags_to_posts'] = 'tags_to_posts';
$config['sidebar'] = 'sidebar';
$config['templates'] = 'templates';
$config['marital_stat'] = 'marital_stat';
$config['languages'] = 'languages';
$config['contactus'] = 'contactus';
$config['users'] = 'users';
$config['users_groups'] = 'users_groups';
$config['groups'] = 'groups';
$config['login_attempts'] = 'login_attempts';
$config['aauth_users'] = 'aauth_users';
$config['aauth_groups'] = 'aauth_groups';
$config['aauth_user_to_group'] = 'aauth_user_to_group';
$config['useralloc'] = 'useralloc';
$config['help'] = 'help';
$config['profile'] = 'profile';
$config['forms'] = 'forms';
$config['autocomplete'] = 'autocomplete';
$config['district'] = 'district';
$config['districtunit'] = 'districtunit';
$config['mekhala'] = 'mekhala';
$config['mekhalaunit'] = 'mekhalaunit';
$config['units'] = 'units';
$config['localunit'] = 'localunit';
$config['members'] = 'members';
$config['lsgi'] = 'lsgi';
$config['members'] = 'members';
$config['active_members'] = 'active_members';
$config['lsgi_orig'] = 'lsgi_orig';
$config['edu_stat'] = 'edu_stat';
$config['job_subsect'] = 'job_subsect';
$config['aoi'] = 'aoi';
$config['blood_grp'] = 'blood_grp';
$config['new_members'] = 'new_members';
$config['renewed_members'] = 'renewed_members';
$config['memb_stat'] = 'memb_stat';
$config['renewal_process'] = 'renewal_process';
$config['memno'] = 'memno';
$config['way_of_asso'] = 'way_of_asso';
$config['missing_units'] = 'missing_units';
$config['aoint'] = 'aoint';
$config['wasso'] = 'wasso';
$config['contact'] = 'contact';
$config['approval_log'] = 'approval_log';
$config['ob_pos'] = 'ob_pos';

/* End of file database_tables.php */
/* Location: ./application/config/database_tables.php */
