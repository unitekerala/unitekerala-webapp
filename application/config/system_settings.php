<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

/*
|--------------------------------------------------------------------------
|Site System Settings
|--------------------------------------------------------------------------
|

|
 */

$config['admin_email']         = 'ranjith.sajeev@gmail.com';
$config['email_title']         = 'Admin Email';
$config['site_status']         = '1'; //1-online, 0-offline
$config['renew_status']         = 1; //1-enabled, 0-disabled
$config['new_status']         = 1; //1-enabled, 0-disabled
$config['site_offline_reason'] = 'Website taken offline for maintanance. Will be back soon';

$config['blog_title']          = 'KSSP Membership';
$config['blog_description']    = 'KSSP Membership Management System';
$config['blog_keywords']       = 'KSSP, Membership, Members';
$config['allow_registrations'] = 0; // Do not allow user registrations.
$config['no_of_items_in_list'] = 25; // Number of Items in a list. Pagination Follows.

//SEO Module
$config['meta_description']    = 'KSSP Membership Management System';
$config['meta_keywords']       = 'KSSP, Membership, Members';

$config['posts_per_page']      = 25;

/* End of file system_settings.php */
/* Location: ./system/application/config/system_settings.php */
