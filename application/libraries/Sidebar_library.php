<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sidebar_library
{
    public function __construct()
    {
        if (!isset($this->CI)) {
            $this->CI = &get_instance();
        }
    }

    public function sidebar_elements()
    {
        $elements = array();
        if (true == $this->CI->access_library->is_statesec()) {
            $elements['dashboard'] = array('method' => 'method', 'router' => 'dashboard', 'url' => 'state/dashboard', 'faico' => 'dashboard', 'span' => 'dashboard');
            $elements['changepass'] = array('method' => 'method', 'router' => 'changepass', 'url' => 'user/changepass', 'faico' => 'pencil-square-o', 'span' => 'changepass');
            $elements['logout'] = array('method' => 'class', 'router' => 'logout', 'url' => 'user/logout', 'faico' => 'arrow-right', 'span' => 'logout');
            $elements['memlist'] = array('method' => 'class', 'router' => 'memlist', 'url' => 'user/statesec/memlist', 'faico' => 'users', 'span' => 'memlist');
            $elements['memcreation'] = array('method' => 'class', 'router' => 'memcreation', 'url' => 'user/statesec/memcreation', 'faico' => 'user-plus', 'span' => 'memcreation');
            $elements['memrenew'] = array('method' => 'class', 'router' => 'memrenew', 'url' => 'user/statesec/memrenew', 'faico' => 'refresh', 'span' => 'memrenew');
            $elements['memtransfer'] = array('method' => 'class', 'router' => 'memtransfer', 'url' => 'user/statesec/memtransfer', 'faico' => 'rocket', 'span' => 'memtransfer');
            $elements['memreview'] = array('method' => 'class', 'router' => 'memreview', 'url' => 'user/statesec/memreview', 'faico' => 'street-view', 'span' => 'memreview');
            $elements['memapprove'] = array('method' => 'class', 'router' => 'memapprove', 'url' => 'user/statesec/mempprove', 'faico' => 'child', 'span' => 'memapprove');
        } elseif (true == $this->CI->access_library->is_statetresh()) {
            $elements['dashboard'] = array('method' => 'method', 'router' => 'dashboard', 'url' => 'state/dashboard', 'faico' => 'dashboard', 'span' => 'dashboard');
            $elements['memapprove'] = array('method' => 'class', 'router' => 'memapprove', 'url' => 'user/statetresh/mempprove', 'faico' => 'child', 'span' => 'memapprove');
            $elements['memlist'] = array('method' => 'class', 'router' => 'memlist', 'url' => 'user/statetresh/memlist', 'faico' => 'users', 'span' => 'memlist');
            $elements['memrenew'] = array('method' => 'class', 'router' => 'memrenew', 'url' => 'user/statetresh/memrenew', 'faico' => 'refresh', 'span' => 'memrenew');
            $elements['memreview'] = array('method' => 'class', 'router' => 'memreview', 'url' => 'user/statetresh/memreview', 'faico' => 'street-view', 'span' => 'memreview');
            $elements['memcreation'] = array('method' => 'class', 'router' => 'memcreation', 'url' => 'user/statetresh/memcreation', 'faico' => 'user-plus', 'span' => 'memcreation');
            $elements['memtransfer'] = array('method' => 'class', 'router' => 'memtransfer', 'url' => 'user/statetresh/memtransfer', 'faico' => 'rocket', 'span' => 'memtransfer');
            $elements['profile'] = array('method' => 'method', 'router' => 'profile', 'url' => 'user/profile', 'faico' => 'user-circle-o', 'span' => 'profile');
            $elements['changepass'] = array('method' => 'method', 'router' => 'changepass', 'url' => 'user/changepass', 'faico' => 'pencil-square-o', 'span' => 'changepass');
            $elements['logout'] = array('method' => 'class', 'router' => 'logout', 'url' => 'user/logout', 'faico' => 'arrow-right', 'span' => 'logout');
        } elseif (true == $this->CI->access_library->is_distsec()) {
            $elements['dashboard'] = array('method' => 'method', 'router' => 'dashboard', 'url' => 'user/distsec/dashboard', 'faico' => 'dashboard', 'span' => 'dashboard');
            $elements['changepass'] = array('method' => 'method', 'router' => 'changepass', 'url' => 'user/changepass', 'faico' => 'pencil-square-o', 'span' => 'changepass');
            $elements['memapprove'] = array('method' => 'class', 'router' => 'memapprove', 'url' => 'user/distsec/mempprove', 'faico' => 'child', 'span' => 'memapprove');
            $elements['logout'] = array('method' => 'class', 'router' => 'logout', 'url' => 'user/logout', 'faico' => 'arrow-right', 'span' => 'logout');
            $elements['memlist'] = array('method' => 'class', 'router' => 'memlist', 'url' => 'user/distsec/memlist', 'faico' => 'users', 'span' => 'memlist');
            $elements['memrenew'] = array('method' => 'class', 'router' => 'memrenew', 'url' => 'user/distsec/memrenew', 'faico' => 'refresh', 'span' => 'memrenew');
            $elements['memreview'] = array('method' => 'class', 'router' => 'memreview', 'url' => 'user/distsec/memreview', 'faico' => 'street-view', 'span' => 'memreview');
            $elements['memcreation'] = array('method' => 'class', 'router' => 'memcreation', 'url' => 'user/distsec/memcreation', 'faico' => 'user-plus', 'span' => 'memcreation');
        } elseif (true == $this->CI->access_library->is_disttresh()) {
            $elements['dashboard'] = array('method' => 'method', 'router' => 'dashboard', 'url' => 'district/dashboard', 'faico' => 'dashboard', 'span' => 'dashboard');
            $elements['memapprove'] = array('method' => 'class', 'router' => 'memapprove', 'url' => 'district/membership/approvemembership', 'faico' => 'street-view', 'span' => 'memapprove');
            $elements['memlist'] = array('method' => 'class', 'router' => 'memlist', 'url' => 'district/membership/listrenewed', 'faico' => 'users', 'span' => 'memlist');
            $elements['memnewlist'] = array('method' => 'class', 'router' => 'memnewlist', 'url' => 'district/membership/listmembers', 'faico' => 'users', 'span' => 'memnewlist');
            $elements['profile'] = array('method' => 'method', 'router' => 'profile', 'url' => 'user/profile', 'faico' => 'user-circle-o', 'span' => 'profile');
            $elements['changepass'] = array('method' => 'method', 'router' => 'changepass', 'url' => 'user/changepass', 'faico' => 'pencil-square-o', 'span' => 'changepass');
            $elements['logout'] = array('method' => 'class', 'router' => 'logout', 'url' => 'user/logout', 'faico' => 'arrow-right', 'span' => 'logout');
        } elseif (true == $this->CI->access_library->is_mekhalasec()) {
            $elements['logout'] = array('method' => 'class', 'router' => 'logout', 'url' => 'user/logout', 'faico' => 'arrow-right', 'span' => 'logout');
            $elements['memrenew'] = array('method' => 'class', 'router' => 'memrenew', 'url' => 'user/mekhalasec/memrenew', 'faico' => 'refresh', 'span' => 'memrenew');
            $elements['memreview'] = array('method' => 'class', 'router' => 'memreview', 'url' => 'user/mekhalasec/memreview', 'faico' => 'street-view', 'span' => 'memreview');
            $elements['memlist'] = array('method' => 'class', 'router' => 'memlist', 'url' => 'user/mekhalasec/memlist', 'faico' => 'users', 'span' => 'memlist');
            $elements['memcreation'] = array('method' => 'class', 'router' => 'memcreation', 'url' => 'user/mekhalasec/memcreation', 'faico' => 'user-plus', 'span' => 'memcreation');
            $elements['dashboard'] = array('method' => 'method', 'router' => 'dashboard', 'url' => 'mekhala/dashboard', 'faico' => 'dashboard', 'span' => 'dashboard');
            $elements['changepass'] = array('method' => 'method', 'router' => 'changepass', 'url' => 'user/changepass', 'faico' => 'pencil-square-o', 'span' => 'changepass');
        } elseif (true == $this->CI->access_library->is_mekhalatresh()) {
            $elements['dashboard'] = array('method' => 'method', 'router' => 'dashboard', 'url' => 'mekhala/dashboard', 'faico' => 'dashboard', 'span' => 'dashboard');
            $elements['memapprove'] = array('method' => 'class', 'router' => 'memapprove', 'url' => 'mekhala/membership/approvemembership', 'faico' => 'street-view', 'span' => 'memapprove');
            $elements['memlist'] = array('method' => 'class', 'router' => 'memlist', 'url' => 'mekhala/membership/listrenewed', 'faico' => 'users', 'span' => 'memlist');
            $elements['memnewlist'] = array('method' => 'class', 'router' => 'memnewlist', 'url' => 'mekhala/membership/listmembers', 'faico' => 'users', 'span' => 'memnewlist');
            $elements['unitlist'] = array('method' => 'class', 'router' => 'unitlist', 'url' => 'mekhala/unit/list_unit', 'faico' => 'puzzle-piece', 'span' => 'unitlist');
            $elements['profile'] = array('method' => 'method', 'router' => 'profile', 'url' => 'user/profile', 'faico' => 'user-circle-o', 'span' => 'profile');
            $elements['changepass'] = array('method' => 'method', 'router' => 'changepass', 'url' => 'user/changepass', 'faico' => 'pencil-square-o', 'span' => 'changepass');
            $elements['logout'] = array('method' => 'class', 'router' => 'logout', 'url' => 'user/logout', 'faico' => 'power-off', 'span' => 'logout');
        } elseif (true == $this->CI->access_library->is_unitsec()) {
            $elements['dashboard'] = array('method' => 'method', 'router' => 'dashboard', 'url' => 'unit/dashboard', 'faico' => 'dashboard', 'span' => 'dashboard');
            $elements['memcreation'] = array('method' => 'class', 'router' => 'memcreation', 'url' => 'unit/membership/newmember', 'faico' => 'user-plus', 'span' => 'memcreation');
            $elements['memrenew'] = array('method' => 'class', 'router' => 'memrenew', 'url' => 'unit/reports/renewal', 'faico' => 'address-book-o', 'span' => 'memrenew');
            $elements['memclose'] = array('method' => 'class', 'router' => 'memclose', 'url' => 'unit/membership/closemembership', 'faico' => 'user-times', 'span' => 'memclose');
            $elements['userprofile'] = array('method' => 'class', 'router' => 'userprofile', 'url' => 'user/profile', 'faico' => 'user', 'span' => 'userprofile');
            $elements['changepass'] = array('method' => 'method', 'router' => 'changepass', 'url' => 'unit/changepass', 'faico' => 'pencil-square-o', 'span' => 'changepass');
            $elements['logout'] = array('method' => 'class', 'router' => 'logout', 'url' => 'user/logout', 'faico' => 'power-off', 'span' => 'logout');
        } elseif (true == $this->CI->access_library->is_user()) {
            $elements['dashboard'] = array('method' => 'method', 'router' => 'dashboard', 'url' => 'user/user/dashboard', 'faico' => 'dashboard', 'span' => 'Dashboard');
            $elements['changepass'] = array('method' => 'method', 'router' => 'changepass', 'url' => 'user/user/houses_list', 'faico' => 'home', 'span' => 'Houses');
            $elements['blog'] = array('method' => 'class', 'router' => 'blog', 'url' => 'blog/posts', 'faico' => 'book', 'span' => 'Blog');
        } elseif (true == $this->CI->access_library->is_admin()) {
            $elements['dashboard'] = array('method' => 'method', 'router' => 'dashboard', 'url' => 'admin/dashboard', 'faico' => 'dashboard', 'span' => 'Dashboard');
            $elements['changepass'] = array('method' => 'method', 'router' => 'changepass', 'url' => 'admin/members', 'faico' => 'users', 'span' => 'Members');
            $elements['blog'] = array('method' => 'class', 'router' => 'blog', 'url' => 'blog/posts', 'faico' => 'book', 'span' => 'Blog');
        }

        return $elements;
    }
}
