<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class System_library
{
    // Public properties
    public $settings = array();
    // Protected or private properties
    protected $_table;

    // Constructor
    public function __construct()
    {
        if (!isset($this->CI)) {
            $this->CI = &get_instance();
        }

        // Load needed models, libraries, helpers and language files
        //$this->CI->load->library('user_agent');

        $this->CI->config->load('database_tables', true);
        $this->CI->config->load('image_settings', true);

        $this->_table = $this->CI->config->item('database_tables');
        //$this->CI->config->set_item('language', $this->get_default_language());

        $this->CI->config->load('system_settings', true);
        $this->settings = $this->CI->config->item('system_settings');
    }

    // Public methods

    public function check_site_status()
    {
        $this->_system = $this->CI->config->item('system_settings');
        if ($this->_system['site_status'] == 0) {
            $data['offline_reason'] = $this->_system['site_offline_reason'];
            $this->CI->load->view('template/pages/offline', $data);
            die();
        }
    }
    public function check_renew_status()
    {
        $this->_system = $this->CI->config->item('system_settings');
        return $this->_system['renew_status'];
    }
    public function check_new_status()
    {
        $this->_system = $this->CI->config->item('system_settings');
        return $this->_system['new_status'];
    }

    public function generate_social_bookmarking_links($post_url, $post_title)
    {
        $links = '';

        if ($this->settings['enable_digg'] == 1) {
            $links = '<a href="http://digg.com/submit?phase=2&amp;url='.$post_url.'&amp;title='.$post_title.'" target="_blank">Digg</a>';
        }

        if ($this->settings['enable_technorati'] == 1) {
            $links .= ($links)?' | ':'';
            $links .= '<a href="http://technorati.com/faves?add='.$post_url.'" target="_blank">Technorati</a>';
        }

        if ($this->settings['enable_delicious'] == 1) {
            $links .= ($links)?' | ':'';
            $links .= '<a href="http://del.icio.us/post?url='.$post_url.'&amp;title='.$post_title.'" target="_blank">del.icio.us</a>';
        }

        if ($this->settings['enable_stumbleupon'] == 1) {
            $links .= ($links)?' | ':'';
            $links .= '<a href="http://www.stumbleupon.com/submit?url='.$post_url.'&amp;title='.$post_title.'" target="_blank">Stumbleupon</a>';
        }

        if ($this->settings['enable_reddit'] == 1) {
            $links .= ($links)?' | ':'';
            $links .= '<a href="http://reddit.com/submit?url='.$post_url.'&amp;title='.$post_title.'" target="_blank">reddit</a>';
        }

        if ($this->settings['enable_furl'] == 1) {
            $links .= ($links)?' | ':'';
            $links .= '<a href="http://www.furl.net/storeIt.jsp?t='.$post_title.'&amp;u='.$post_url.'" target="_blank">Furl</a>';
        }

        return $links;
    }
    public function load($page, $data = null, $admin = false)
    {

        $data['page'] = $page;

        if ($admin == true) {
            $this->CI->load->view('templates/admin/container', $data);
        } else {
            $this->CI->load->view('templates/frontend/container', $data);
        }
    }
    public function load_user($page, $data = null, $admin = false)
    {

        $data['page'] = $page;

        if ($admin == true) {
            $this->CI->load->view('templates/admin/container', $data);
        } else {
            $this->CI->load->view('templates/user/container', $data);
        }
    }
    public function load_normal($page, $data = null)
    {

        $this->CI->load->view('templates/frontend/pages/'.$page, $data);
    }
    public function load_direct($page, $data = null)
    {
        //$template = $this->get_default_template();

        $this->CI->load->view('templates/frontend/'.$page, $data);
    }
    public function generate_xml($file, $page, $data = null)
    {
        $string = $this->CI->load->view('templates/frontend/'.$page, $data, true);
        $xmlfolder='/var/www/html/sitemap/';
        echo $xmlfolder.$file;
        $op=file_put_contents($xmlfolder.$file, $string);
        if ($op) {
            echo "Site Map File written successfully at - ".$file;
        } else {
            echo "Error in File Creation Operation";
        }
    }
    //  Windows 1251 -> Unicode
}

/* End of file System.php */
/* Location: ./application/libraries/System.php */
