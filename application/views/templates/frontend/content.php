<?php if (!isset($homeslider)){$homeslider=false;} ?>
<?php $this->load->view('templates/frontend/layout/menu');?>
<?php  if ($homeslider===true) {
    $this->load->view('templates/frontend/pages/'.$page);
} else { ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <?php $this->load->view('templates/frontend/pages/'.$page);?>
            </div>
            <div class="col-md-4">
                <?php $this->load->view('templates/frontend/layout/sidebar');?>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php $this->load->view('templates/frontend/layout/footer');?>
