<footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        Powered by <b><a href="http://kssp.in/">KSSP</a></b>
      </div>
      <strong>Copyright &copy; 2018<a href="https://keralapadanam.com"> Keralapadanam 2.0</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
