<!DOCTYPE html>
<html lang="en">
<head>
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>static/icon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>static/icon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>static/icon/favicon-16x16.png">
        <link rel="mask-icon" href="<?php echo base_url();?>static/icon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="manifest" href="<?php echo base_url();?>static/icon/manifest.json">

        <title><?php if (isset($post['title'])) {
            echo $post['title'];
               } elseif (isset($page_data['title'])) {
    echo $page_data['title'];
} else {
    echo $this->system_library->settings['blog_title'];
}?></title>

        <meta charset="utf-8">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo base_url();?>static/icon/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="MobileOptimized" content="width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" property="og:description" content="<?php if (isset($page_data['description'])) {
        echo ($page_data['description']);
                                                                } else {
    echo ($this->system_library->settings['blog_description']);
}?>">

    <link href="<?php echo base_url();?>static/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>static/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>static/css/AdminLTE.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>static/css/skin-green.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>static/css/main.css" rel="stylesheet">
    <script src="<?php echo base_url();?>static/js/jquery.min.js" type="text/javascript"></script>
</head>

<body class="hold-transition skin-green layout-top-nav">
<div class="wrapper">
<?php $this->load->view('templates/frontend/content');?>

</div>
<!--   core js files    -->
<script src="<?php echo base_url();?>static/js/bootstrap.min.js" type="text/javascript"></script>

<!--  js library for devices recognition -->
<script type="text/javascript" src="<?php echo base_url();?>static/js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>static/js/adminlte.min.js"></script>

<!-- for sidebar-->
<script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.slimscroll.js"></script>


<!-- Default Statcounter code for Keralapadanam.com
https://keralapadanam.com -->
<script type="text/javascript">
var sc_project=11694327;
var sc_invisible=0;
var sc_security="065e9a9a";
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="Web Analytics"
href="http://statcounter.com/" target="_blank"><img
class="statcounter"
src="//c.statcounter.com/11694327/0/065e9a9a/0/" alt="Web
Analytics"></a></div></noscript>
<!-- End of Statcounter Code -->

</body>
</html>
