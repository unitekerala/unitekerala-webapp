<section class="content-header">
	<h1>Progress Demo</h1>
	
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home');
			?></a> <span class="divider">/</span></li>
			<li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>homepage/progress">Progress</a> <span class="divider">/</span></li>
			</ol>
		</section>
		<section class="content">
		<h2>Overall Progress</h2>
			<div class="progress">
			  <div class="progress-bar progress-bar-success progress-bar-striped" style="width: 35%">
			    <span class="sr-only">35% Complete (success)</span>
			  </div>
			  <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 20%">
			    <span class="sr-only">20% Complete (warning)</span>
			  </div>
			  <div class="progress-bar progress-bar-danger" style="width: 10%">
			    <span class="sr-only">10% Complete (danger)</span>
			  </div>
			</div>
			<p> Total Completed cells : 2000</p>
			<p> Total Inprogress Cells : 1500</p>
			<p> Total Not Started Cells : 2500</p>
		<h2>District Wise Progress</h2>
		<table class="table table-striped table-responsive">
			<tr><td width="20%">Thrissur</td><td>
				<div class="progress">
 				<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
    				60%
  				</div>
				</div>
				</td></tr>
			<tr><td width="20%">Ernakulam</td><td>
				<div class="progress">
 				<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
    				40%
  				</div>
				</div>
				</td></tr>
			<tr><td width="20%">Palakkad</td><td>
				<div class="progress">
 				<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
    				25%
  				</div>
				</div>
				</td></tr>
		</table>
		<h2>Cell Wise Progress</h2>
		<table>
		<tr>
		<td>
		</table>
		<table class="table">
			<tr>
				<td style="width:100px; text-align:center;">1</td>
				<td style="width:100px; text-align:center;">2</td>
				<td style="width:100px; text-align:center;">3</td>
				<td style="width:100px; text-align:center;">4</td>
				<td style="width:100px; text-align:center;">5</td>
				<td  style="width:100px; text-align:center;" class = 'text-center col-md-4 success'>6</td>
				<td style="width:100px; text-align:center;">7</td>
				<td style="width:100px; text-align:center;">8</td>
				<td style="width:100px; text-align:center;">9</td>
				<td style="width:100px; text-align:center;" class = 'text-center col-md-4 danger'>10</td>
			</tr>
			<tr>
				<td style="width:100px; text-align:center;">11</td>
				<td style="width:100px; text-align:center;">12</td>
				<td style="width:100px; text-align:center;">13</td>
				<td style="width:100px; text-align:center;">14</td>
				<td style="width:100px; text-align:center;" class = 'text-center col-md-4 success'>15</td>
				<td style="width:100px; text-align:center;">16</td>
				<td style="width:100px; text-align:center;">17</td>
				<td style="width:100px; text-align:center;">18</td>
				<td style="width:100px; text-align:center;">19</td>
				<td style="width:100px; text-align:center;">20</td>
			</tr>
			<tr>
				<td style="width:100px; text-align:center;">21</td>
				<td style="width:100px; text-align:center;">22</td>
				<td style="width:100px; text-align:center;">23</td>
				<td style="width:100px; text-align:center;">24</td>
				<td style="width:100px; text-align:center;">25</td>
				<td style="width:100px; text-align:center;" class = 'text-center col-md-4 success'>26</td>
				<td style="width:100px; text-align:center;">27</td>
				<td style="width:100px; text-align:center;">28</td>
				<td style="width:100px; text-align:center;">29</td>
				<td style="width:100px; text-align:center;">30</td>
			</tr>
			<tr>
				<td style="width:100px; text-align:center;" class = 'text-center col-md-4 success'>31</td>
				<td style="width:100px; text-align:center;">32</td>
				<td style="width:100px; text-align:center;">33</td>
				<td style="width:100px; text-align:center;">34</td>
				<td style="width:100px; text-align:center;">35</td>
				<td style="width:100px; text-align:center;">36</td>
				<td style="width:100px; text-align:center;">37</td>
				<td style="width:100px; text-align:center;">38</td>
				<td style="width:100px; text-align:center;">39</td>
				<td style="width:100px; text-align:center;">40</td>
			</tr>
			<tr>
				<td style="width:100px; text-align:center;">41</td>
				<td style="width:100px; text-align:center;">42</td>
				<td style="width:100px; text-align:center;">43</td>
				<td style="width:100px; text-align:center;">44</td>
				<td style="width:100px; text-align:center;">45</td>
				<td style="width:100px; text-align:center;">46</td>
				<td style="width:100px; text-align:center;">47</td>
				<td style="width:100px; text-align:center;">48</td>
				<td style="width:100px; text-align:center;">49</td>
				<td style="width:100px; text-align:center;">50</td>
			</tr>
		</table>
		</section>