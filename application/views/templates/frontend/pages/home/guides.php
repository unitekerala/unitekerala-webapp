
            <br/>
            <h1>Instructions for Kerala Padanam 2.0</h1>
            <p>Videos about different topics in Kerala Padanam listed here.These are the instructions for volunteers of Kerala Padanam</p>
            <div class="row">
            <h2>How to Use Keralapadanam 2.0 Website</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/52jE4J_i4-k?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>How to Enter Data into Keralapadanam Website</p></div>
            </div>
            <hr/>
            <div class="row">
            <h2>Common Instructions about Kerala Padanam 2.0</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/fQWJufW1xfw?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>Common Instructions for Kerala Padanam 2.0 by K.K Janardanan</p></div>
            </div>
            <hr/>
            <div class="row">
            <h2>Income Expenditure Form</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/0aNO65sxPo8?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>About Income and Expenditure form by Dr. K.P. Aravindan</p></div>
            </div><hr/>
            <div class="row">
            <h2>About Different Data Types in Kerala Padanam</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/KzGZZfft-vY?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>About Different Data Types and how to collect data in Kerala Padanam by Dr. K.P. Aravindan</p></div>
            </div>
            <hr/>
            <div class="row">
            <h2>About Health Data</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/3198cBRvT14?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>About Health Data in Kerala Padanam by Dr. K.P. Aravindan</p></div>
            </div>
            <hr/>
            <div class="row">
            <h2>About the Bias in data</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/LQalNGmA-F4?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>About the bias in data collection in Kerala Padanam by Dr. K.P. Aravindan</p></div>
            </div>
            <hr/>
            <div class="row">
            <h2>About data in Parents form</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/PrveQ6wGsWE?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>About data collection of parents form in Kerala Padanam by Dr. K.P. Aravindan</p></div>
            </div>
            <hr/>
        