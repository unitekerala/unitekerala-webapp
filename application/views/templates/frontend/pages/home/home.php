<?php
  $homedata = array();
foreach ($home as $homeval) {
    $key = $homeval['name'];
    $value = $homeval['value'];
    $homedata[$key] = $value;
}
?>
<!-- Debugging helper -->
<!-- <pre><?php print_r($homedata); ?></pre> -->

<div class="content-wrapper">
  <div class="jumbotron bgimg-1 filter filter-color-purple">
    <div class="container">
    <div class="title-area">
      <h1 class="display-3"><?php echo $homedata['section1title']; ?></h1>
      <p><?php echo $homedata['section1desc']; ?></p>
      <p><a class="btn btn-danger btn-lg btn-flat" href="<?php echo base_url();?>about" role="button"><?php echo $homedata['section1urltext']; ?> &raquo;</a></p>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="title-area">
            <h2><?php echo $homedata['section2title']; ?></h2>
            <div class="separator"></div>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="title-area">

        <p class="about-description"><?php echo $homedata['section2desc']; ?></p>
      </div>
    </div>
    <div class="row blocks">
      <div class="col-md-4">
        <div class="info-icon">
          <div class="icon text-danger">
            <i class="fa <?php echo $homedata['section3icon1']; ?>"></i>
          </div>
          <h3><?php echo $homedata['section3title1']; ?></h3>
          <p class="description"><?php echo $homedata['section3text1']; ?></p>
	  <p><a class="btn btn-primary" href="<?php echo base_url('page/viewpage/instructions'); ?>" role="button"><?php echo $homedata['section3urltext1']; ?> &raquo;</a></p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="info-icon">
          <div class="icon text-danger">
            <i class="fa <?php echo $homedata['section3icon2']; ?>"></i>
          </div>
          <h3><?php echo $homedata['section3title2']; ?></h3>
          <p class="description"><?php echo $homedata['section3text2']; ?></p>
	  <p><a class="btn btn-primary" href="<?php echo site_url('page/viewpage/data'); ?>" role="button"><?php echo $homedata['section3urltext2']; ?> &raquo;</a></p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="info-icon">
          <div class="icon text-danger">
            <i class="fa <?php echo $homedata['section3icon3']; ?>"></i>
          </div>
          <h3><?php echo $homedata['section3title3']; ?></h3>
          <p class="description"><?php echo $homedata['section3text3']; ?></p>
	  <p><a class="btn btn-primary" href="<?php echo site_url('page/viewpage/guide');?>" role="button"><?php echo $homedata['section3urltext3']; ?> &raquo;</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
