
            <br/>
            <h1>Videos about Kerala Padanam</h1>
            <p>Videos about different topics about Kerala Padanam listed here.</p>
            <div class="row">
            <h2>About Kerala Padanam</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/Nng36wkvRLE?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>A Video about Kerala Padanam 2 by Dr. K.P. Aravindan, Editor of Kerala Padanam 1 Report</p></div>
            </div>
            <hr/>
            <div class="row">
            <h2>Why 6000 Samples taken?</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/QWgrZEouERI?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>Why 6000 samples are taken for Kerala Padanam by Dr. K.P. Aravindan, Editor of Kerala Padanam 1 Report</p></div>
            </div><hr/>
            <div class="row">
            <h2>About various surveys by KSSP</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/zxQ47Bn_VO8?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>A Video about various surveys conducted by KSSP by T Gangadharan, President of Kerala Sasthrasahithya Parishad</p></div>
            </div><hr/>
            <div class="row">
            <h2>Towards Kerala Padanam 2.0</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/GAfY3rUqNM0?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>Towards Kerala Padanam 2.0 by Prof. T.P. Kunjikannan</p></div>
            </div>
            <hr/>
            <div class="row">
            <h2>About a Local Study Team</h2>
            <div class="col-md-6"> 
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"  src="//www.youtube.com/embed/xvQDeW1ZhKE?autoplay=0&showinfo=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6"> <p>About a local study team by K.K. Janardanan</p></div>
            </div>
            <hr/>
       