<?php  if ( ! defined('BASEPATH')) {exit('No direct script access allowed');}

$lang['dashboard'] = 'Dashboard';
$lang['profile']                = "Profile";
$lang['no_profile']             = "No Profile Available to View";
$lang['view_profile_description']               = "View Profile details";
$lang['edit_profile']               = "Edit Profile";
$lang['no_house_allocated']               = "No House Allocated";



$lang['profile_username']                = "Username";



$lang['prfile_phonenumber']                = "Phone Number";



$lang['profile_address']                = "Address";
$lang['user_register']                = "Complete the basic info before entering to forms";



$lang['profile_district']                = "District";



$lang['profile_kssp_unit']                = "KSSP Unit";
$lang['profile_panchayath']                = "Panchayath";


$lang['profile_profile']               = "Profile Information";
$lang['profile_logout']               = "Logout";
$lang['profile_user_status']               = "House Status";
$lang['profile_editprofile']               = "Save Profile";
$lang['profile_changepass']               = "Change Password";
$lang['profile_enterpass']               = "Enter New Password";
$lang['profile_submitpassword']               = "Submit";

$lang['profile_successfully_edited']                = "Profile is successfully edited";

$lang['verification_error']              = "Error in verifying the house, please try again!"; 
